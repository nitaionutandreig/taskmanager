$(document).ready(function (){
    var table = $('#task-datatable').DataTable({
    	buttons: [
	        {
	            text: 'Add Task',
	            action: function ( e, dt, node, config ) {
	                $('#add-task-modal').modal("show");
	            }
	        }
    	]
    });

    table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

    $("#add-new-task").on('click', function() {
    	var taskName = $("#add-name-input").val();
  		var taskDesc = $("#add-desc-input").val();
  		var taskStatus = $("#add-status option:selected").text();
      var csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
  		if (taskName != "" && taskDesc != "") {
  			$.post('add_new_task/', {'name': taskName, 'desc': taskDesc, 'status': taskStatus, 'csrfmiddlewaretoken': csrf_token})
  			.done(function(data) {
                  $("#close-modal").click();
                  location.reload();
              })
  			.fail(function(response) {
  			    alert('Error: ' + response.responseText);
              });
  		} else {
  			alert('Please input a name and a description!')
		}
    });

    $('#task-datatable tbody').on("click", ".edit-task", function (event) {
      var taskName = $(this).data('task-name');
      var taskDesc = $(this).data('task-desc');
      var taskStatus = $(this).data('task-status');
      $(".modal-body").attr('name', taskName);
      $("#edit-name-input").attr('value', taskName);
      $("#edit-desc-input").attr('value', taskDesc);
      $("#edit-status").val(taskStatus);
    });

    $('#save-edit-task').on('click', function() {
      var initTaskName = $(".modal-body").attr('name');
      var taskName = $('#edit-name-input').val();
      var taskDesc = $('#edit-desc-input').val();
      var taskStatus = $("#edit-status option:selected").text();
      var csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
      $.post('edit_task/', {'init_name': initTaskName, 'name': taskName, 'desc': taskDesc, 'status': taskStatus, 'csrfmiddlewaretoken': csrf_token})
  	  .done(function(data) {
  	    $("#close-modal").click();
  	    location.reload();
  	  });
    });

    $('#task-datatable tbody').on("click", ".delete-task", function (event) {
      var taskName = $(this).data('task-name');
      $(".modal-body").attr('name', taskName);
    });

    $(".confirm-delete-task").on('click', function() {
    	var taskName = $('.modal-body').attr('name');
      var csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
    	$.post('delete_task/', {'name': taskName, 'csrfmiddlewaretoken': csrf_token})
    	.done(function(data) {
	        $("#close-modal").click();
	        location.reload();
        });
    });    

    $(".mark-done").on('click', function() {
    	var taskName = $(this).data('task-name');
      var csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;
    	$.post('mark_done/', {'name': taskName, 'csrfmiddlewaretoken': csrf_token})
    	.done(function(data) {
        	location.reload();
        });
    });

});
