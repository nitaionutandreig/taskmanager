from django.test import TestCase
from django.urls import reverse
from TaskList.models import Task

class AuthorListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_tasks = 13
        for task_num in range(number_of_tasks):
            Task.objects.create(	
            					  name='testtask %s' % task_num, 
					              description='testdescription %s' % task_num,
					              status=False
					             )
           
    def test_root_url_redirects(self): 
        resp = self.client.get('/') 
        self.assertEqual(resp.status_code, 302)

    def test_login_url_redirects(self): 
        resp = self.client.get('/login') 
        self.assertEqual(resp.status_code, 301)

    def test_logout_url_redirects(self): 
        resp = self.client.get('/logout') 
        self.assertEqual(resp.status_code, 301)


    def test_add_url_redirects(self): 
        resp = self.client.get('/add_new_task') 
        self.assertEqual(resp.status_code, 301)

    def test_edit_url_redirects(self): 
        resp = self.client.get('/edit_task') 
        self.assertEqual(resp.status_code, 301)

    def test_delete_url_redirects(self): 
        resp = self.client.get('/delete_task') 
        self.assertEqual(resp.status_code, 301)

    def test_done_url_redirects(self): 
        resp = self.client.get('/mark_done') 
        self.assertEqual(resp.status_code, 301)