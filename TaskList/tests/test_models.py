from django.test import TestCase
from TaskList.models import Task

class TaskModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Task.objects.create(name="testtask", description="testdescription", status=False, owner="testuser")

    def test_name_label(self):
        task = Task.objects.get(name="testtask")
        field_label = task._meta.get_field("name").verbose_name
        self.assertEquals(field_label,'name')

    def test_description_label(self):
        task = Task.objects.get(name="testtask")
        field_label = task._meta.get_field("description").verbose_name
        self.assertEquals(field_label,'description')

    def test_status_label(self):
        task = Task.objects.get(name="testtask")
        field_label = task._meta.get_field("status").verbose_name
        self.assertEquals(field_label,'status')

    def test_owner_label(self):
        task = Task.objects.get(name="testtask")
        field_label = task._meta.get_field("owner").verbose_name
        self.assertEquals(field_label,'owner')

    def test_name_max_length(self):
        task = Task.objects.get(name="testtask")
        max_length = task._meta.get_field('name').max_length
        self.assertEquals(max_length, 50)

    def test_description_max_length(self):
        task = Task.objects.get(name="testtask")
        max_length = task._meta.get_field('description').max_length
        self.assertEquals(max_length, 200)

    def test_owner_max_length(self):
        task = Task.objects.get(name="testtask")
        max_length = task._meta.get_field('owner').max_length
        self.assertEquals(max_length, 50)
