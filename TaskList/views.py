from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages

from .models import Task

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Invalid Username or Password.')

    return render(request, 'registration/login.html')

@login_required(login_url='login/')
def index(request):
    tasks = Task.objects.all()
    return render(request, 'index.html', {'tasks': tasks})

def add_new_task(request):
    status = True if request.POST['status'] == 'Done' else False
    new_task = Task.objects.create(
                                    name=request.POST['name'], 
                                    description=request.POST['desc'], 
                                    status=status,
                                    owner=request.user.username,
                                    )
    return redirect('/')

def edit_task(request):
    status = True if request.POST['status'] == 'Done' else False
    init_task_name = request.POST['init_name']
    task = Task.objects.get(name=init_task_name)
    if request.POST['name'] != task.name: 
    	task.name = request.POST['name']  
    if request.POST['desc'] != task.description: 
    	task.description = request.POST['desc']
    if status != task.status: 
    	task.status = status 
    task.modified_by = request.user.username
    task.save()
    return redirect('/')

def delete_task(request):
    task_name = request.POST['name']
    task = Task.objects.get(name=task_name).delete()
    return redirect('/')

def mark_done(request):
    task_name = request.POST['name'].encode("utf-8").decode("utf-8")
    task = Task.objects.get(name=task_name)
    task.status = True
    task.modified_by = request.user.username
    task.save()
    return redirect('/')

def logout_user(request):
    """
    Log users out and re-direct them to the main page.
    """
    logout(request)
    return redirect('/')
