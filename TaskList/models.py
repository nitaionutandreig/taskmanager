from django.db import models

class Task(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=200)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True)
    owner = models.CharField(max_length=50)
    modified_by = models.CharField(max_length=50, blank=True)

    # Metadata
    class Meta:
        ordering = ["name"]

    # Return task name and owner
    def __str__(self):
        return "{}: {}".format(self.name, self.owner)