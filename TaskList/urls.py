from django.urls import path

from . import views


urlpatterns = [
	path('', views.index, name='index'),
    path('login/', views.login_user, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('add_new_task/', views.add_new_task, name='add_new_task'),
    path('edit_task/', views.edit_task, name='edit_task'),
    path('delete_task/', views.delete_task, name='delete_task'),
    path('mark_done/', views.mark_done, name='mark_done'),
]
