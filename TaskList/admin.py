from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ["name", "description", "status", "created_at", "owner"]

    # link model Admin to model
    class Meta:
        model = Task

admin.site.register(Task, TaskAdmin)